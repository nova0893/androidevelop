<?php
   session_start();
   if(isset($_SESSION['name'])) {
   header('location:view/home.php'); }
   require_once("config/koneksi.php");
?>

<html>
<head>

  <meta charset="utf-8">
  <title>OASIS</title>

  <link rel="stylesheet" href="assets/backend/bootstrap/css/bootstrap.min.css">
	<style type="text/css">

	@font-face{font-family:"Esphimere";src:url("assets/font/esphimere/Esphimere.otf");}
	@font-face{font-family:"Esphimere";src:url("assets/font/esphimere/Esphimere Bold.otf");font-weight:bold;}
	@font-face{font-family:"Esphimere";src:url("assets/font/esphimere/Esphimere Italic.otf");font-style:italic;}
	*{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;}
	body{margin:0;padding:0;background:url("assets/img/background.png")}
	body,input,select{font-family:"Esphimere";font-size:13px;}
	h1,h2,h3,h4,h5,p{margin:0;}
	.wrapper{width:1025px;margin:0 auto;text-align:center;}
	header,footer{height:100px;}
	#content{background-color:#fff;color:#636363;}
	header{background:url("assets/img/header.png") no-repeat left #82bf34;}
	footer{background:url("assets/img/header.png") no-repeat left #007ec8;}
	#sitetitle{color:#323094;font-size:21px;font-weight:400;padding:15px 0;}
	.bottom-sp{border-bottom:1px solid #e1e1e1;}
	.top-sp{border-top:1px solid #e1e1e1;}
	#content p, #footer p{padding:10px 0;line-height:16px;} 
	#content a{color:#018bd3;text-decoration:none;}
	footer .copyright{padding:30px 0;color:#e6ecef;}


	</style>



</head>
<body><br />
<header> 
           <div class="body" style=" width: 100%; height:100px; margin-left: 15px;">
           
          



    </div> 


    </header>
    <section id="content" >
            <div class="container">

            <div align="center">

	            
            	<br>
	             <img src="assets/img/logo.png"> 
	             <h1 id="sitetitle">Admin OASIS STT-Bandung</h1>

                <?php
    if(isset($_GET['failuser']))
    {
        ?>
        <label>Mohon maaf, username tidak terdaftar.</a></label>
        <?php
    }
    else if(isset($_GET['failpassword']))
    {
        ?>
        <label>Password yang Anda masukkan salah. Silahkan coba lagi</label>
        <?php
    }
    else
    {
        ?>
        <?php
    }
    ?>



</div>
  

   <div 
         <!-- form start -->
         <form name="form-login" method="POST" action="controllers/proseslogin.php">
         
         <div class="form-header">

                      
        
                      
         </div>
         
         <div class="form-body">
                   <div class="input-group">
                   <div class="input-group-addon"><span class="glyphicon glyphicon-user"></span></div>
                   <input name="user" id="user" type="text" class="form-control" placeholder="Username">
                
                   </div>
                   <span class="help-block" id="error"></span>

                   <span class = "help-block" id = "error"> </span>
              </div>
                        
              <div class="form-body">
                        
                   <div class="form-group">
                        <div class="input-group">
                        <div class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></div>
                        <input name="password" id="password" type="password" class="form-control" placeholder="Password">
                     
                        </div>  
                        <span class="help-block" id="error"></span>                    
                   </div>
                            
          
                            
             </div>
                        
                        
            </div>
             
                    
        <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-block btn-flat" name='login' id='login'>Sign In <i class="fa fa-sign-in"></i></button>
                    </div>


            </form>
            <br>
            <br>
           </div>

            </div>


        </div>
   

    	     <footer>
    	<div class="wrapper clearfix">
        	<div align="center">

            	<p class="copyright">&copy; Developed By PPSI-2016</p>
            </div>
        </div>
    </footer>
    </section>

    </div>


 <script src="assets/backend/dist/js/header.js"></script>
  <script src="assets/backend/dist/js/header2.js"></script>



</body>
</html>
            <div class="form-group">