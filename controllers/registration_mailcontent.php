<div id="email-content">
	<p>Selamat Datang,</p>
    <p>Anda telah terdaftar di Online Academics Information System of STT-Bandung.</p>
	<table border="0">
		<tr><td colspan="2"><b>Detail Akun:</b><br /></td></tr>
        <tr style="vertical-align:top;">
            <td>NPM</td>
            <td>: <?php echo $npm; ?></td>
        </tr>
        <tr style="vertical-align:top;">
            <td>Nama Lengkap</td>
            <td>: <?php echo $nama; ?></td>
        </tr>
        <tr style="vertical-align:top;">
            <td>Email</td>
            <td>: <?php echo $email; ?></td>
        </tr>
        <tr style="vertical-align:top;">
            <td>Password</td>
            <td>: <?php echo $password; ?></td>
        </tr>
	</table>
    <p>Selanjutnya, silakan melengkapi data diri anda dengan mengakses halaman <a href="http://oasis.sttbandung.ac.id/">Login</a> berikut.</p>
    <p style="padding-top:10px;">
    Salam Hangat,<br /><br />
    Online Academics Information System (OASIS)<br />
    Sekolah Tinggi Teknologi Bandung<br />
    Jl. Soekarno Hatta No.378 Bandung 40235<br />
    Telp. (022) 5224000
    </p>
</div>