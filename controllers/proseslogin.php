<?php
   session_start();
   require_once("../config/koneksi.php");
   $username = $_POST['user'];
   $pass = $_POST['password'];
   $password = sha1($pass);
   $query = $pdo->prepare("SELECT * FROM ms_admin WHERE username = ?");
   $query->execute(array($username));
   $hasil = $query->fetch();
   if($query->rowCount() == 0) {
    ?>
      <script>
        window.location.href='../index.php?failuser';
        </script>
        <?php
   } else {
     if($password <> $hasil['password']) {
       ?>
    <script>
        window.location.href='../index.php?failpassword';
        </script>
    <?php
     } else {
       $_SESSION['name'] = $hasil['name'];
       header('location:../view/home.php');
     }
   }
?>