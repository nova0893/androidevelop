<?php
	ob_start();
	include '../config/koneksi.php';
	$body = ob_get_clean();

require '../assets/phpmailer/PHPMailerAutoload.php';
$mail = new PHPMailer(true);
try {
	$mail->isSMTP();
	$mail->SMTPDebug = 0;
	$mail->Debugoutput = 'html';
	$mail->Host = "mail.sttbandung.ac.id";
	$mail->Port = 27;
	$mail->SMTPAuth = true;
	$mail->SMTPSecure = "";
	$mail->Username   = "oasis@sttbandung.ac.id";
	$mail->Password   = "pcStr4ng3r";
    $mail->setFrom('oasis@sttbandung.ac.id', 'OASIS STT-Bandung Administrator');
    $mail->addReplyTo('oasis@sttbandung.ac.id', 'OASIS STT-Bandung Administrator');
    $mail->addAddress($email, $nama);
    $mail->Subject = 'User Registration from OASIS STT-Bandung';
    $mail->msgHTML($body);

    $mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';

	if (!$mail->send()) {
		echo "Mailer Error: " . $mail->ErrorInfo;
		exit;
	} else {
		$query = "INSERT INTO `ms_mhs`(`oid`, `str_npm`, `str_nm_mhs`, `str_kd_prodi`, `dte_tgl_lhr`) VALUES (NULL,?,?,?,?)";
		$stmt = $pdo->prepare($query);
		$stmt->execute(array($npm,$nama,$prodi,$tgllhr));
		
		$query = "INSERT INTO `ms_user`(`oid`, `str_reff`, `str_key`, `str_email`, `str_username`, `str_password`, `bol_status`) VALUES (NULL,'ms_mhs',?,?,'',?,'1')";
		$stmt = $pdo->prepare($query);
		$stmt->execute(array($npm,$email,sha1($password)));
		
		header('Location: ../view/home.php');
		exit;
	}
} catch (phpmailerException $e) {
    echo "Mailer Error: " . $e->errorMessage();
	exit;
} catch (Exception $e) {
    echo "Error: " . $e->getMessage();
	exit;
}
?>