<?php 
include '../config/koneksi.php';
?>

<?php
if(!isset($_POST['prodi']) or !isset($_POST['npm']) or !isset($_POST['nama']) or !isset($_POST['email'])){
	header('Location: ../view/home.php');
	exit;
}else{
	$prodi = $_POST['prodi'];
	$npm = $_POST['npm'];
	$nama = $_POST['nama'];
	$email = $_POST['email'];
	if(empty($_POST['tgllhr']) || $_POST['tgllhr']=='0000-00-00'){
		$tgllhr = '';
	}else{
		$x = explode('/',$_POST['tgllhr']);
		$_POST['tgllhr'] = $x[2].'-'.$x[1].'-'.$x[0];
		$tgllhr = date('Y-m-d',strtotime($_POST['tgllhr']));
		
		if($tgllhr=='0000-00-00' || $tgllhr=='1970-01-01'){
			$tgllhr = '';
		}
	}
	
	try{
		$query = "SELECT * FROM `ms_user` A WHERE A.`str_reff`='ms_mhs' AND (A.`str_key`=? OR A.`str_email`=?) AND A.`bol_status`='1'";
		$stmt = $pdo->prepare($query);
		$stmt->execute(array($npm,$email));
		$row_count = $stmt->rowCount();
		
		if(empty($prodi) or empty($npm) or empty($nama) or empty($email)){
			$msgcode = 1;
			
			header('Location: ../view/registration.php');
			exit;
		}else if($row_count>0){
			$msgcode = 2;
			
			header('Location: ../view/registration.php');
			exit;
		}else{			
			include('function.php');
			$password = generateRandomString();
			
			//Mailer
			include('registration_mailer.php');
		}
		
	}catch(PDOException $ex) {
		echo "There are some problems in our database. Please try again later.";
		exit;
	}
}
?>