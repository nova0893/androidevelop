<?php 
	/* Template Name: Double Columns Left */ 
?>

	<?php get_header();?>
			<div class="container">
				<div class="col-md-3">
					<?php get_sidebar('inner-page'); ?>
				</div>
			
				<div class="col-md-9">
					<!-- section -->
					<section role="main">
						<header class="entry-header entry-header-page">
							<h2><?php the_title(); ?></h2>
						</header>
						
						<?php if (have_posts()): while (have_posts()) : the_post(); ?>
				
							<!-- article -->
							<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							
								<?php the_content(); ?>	
								
								<?php comments_template( '', true ); // Remove if you don't want comments ?>
								
								<br class="clear">
								
								<?php edit_post_link(); ?>
								
							</article>
							<!-- /article -->
					
							<?php endwhile; ?>
				
						<?php else: ?>
				
							<!-- article -->
							<article>
								
								<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>
								
							</article>
							<!-- /article -->
				
						<?php endif; ?>
				
					</section>
					<!-- /section -->	
				</div>
			</div>
<?php get_footer();?>