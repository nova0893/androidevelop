<?php get_header();?>
	<div class="content-page tag-page">
		<div class="container">
			<div class="col-md-12">
				<!-- section -->
				<section role="main">
					<p class="title-archive"><?php _e( 'Arsip Tag: ', 'html5blank' ); echo single_tag_title('', false); ?></p>
		
					<?php get_template_part('loop'); ?>
			
					<?php get_template_part('pagination'); ?>
				</section>
				<!-- /section -->
			</div>
		</div>
	</div>
<?php get_footer();?>