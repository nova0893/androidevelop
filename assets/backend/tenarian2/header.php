<!doctype html>
<html <?php language_attributes();?> class="no-js">
<head>
<meta charset="<?php bloginfo('charset');?>">
<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name');?></title>
<!-- meta -->
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<meta name="description" content="<?php bloginfo('description'); ?>">
<meta name="google-site-verification" content="5aRxqSdJtVdGg3N3_0C491jmMm0n9C5KnrUrOzUFHnM" />
<!-- icons -->
<link href="<?php echo get_template_directory_uri(); ?>/ico/favicon.png" rel="shortcut icon">
<link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">
<link href="<?php echo get_template_directory_uri(); ?>/ico/favicon.png" rel="apple-touch-icon-precomposed">
<?php wp_head();?>
<script>
!function(){
conditionizr()
}()
</script>
<script>
$(function(){$(".prodi-tab > li").click(function(){var li = $(this);var tab = li.attr("tab");if(!li.hasClass("active")){var actli = $(".prodi-tab > li.active");var acttab = actli.attr("tab");actli.removeClass("active");li.addClass("active");$(".prodi-ctn[tab='"+acttab+"']").addClass("hide");$(".prodi-ctn[tab='"+tab+"']").removeClass("hide");}});});
</script>
</head>
<body <?php body_class(); ?> id="clexah">
<div class="wraper">
<header class="branding" role="banner">
<div class="container">
<nav class="top-nav-wrap pull-right">
<?php
wp_nav_menu( array(
'menu'              => 'menu-atas',
'theme_location'    => 'menu-atas',
'depth'             => 2,
'container'         => 'div',
'container_class'   => 'top-menu',
'container_id'      => 'nav',
'menu_class'        => 'top-nav',
'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
'walker'            => new wp_bootstrap_navwalker())
);
?>
</nav>
</div>
<div class="container">
<div class="col-md-8">
<ul class="cp-wrap pull-left">
<li>
<i class="fa fa-phone fa-fw"></i> Phone: <span>(022) 522-4000</span>
</li>
<li>
<i class="fa fa-tablet fa-fw"></i> HP/Line/WA:<span> 081-2222-55-777</span>
</li>
<li>
<i class="fa fa-envelope fa-fw"></i> Email: <span>info@sttbandung.ac.id</span>
</li>
</ul>
</div>
<!-- Social Buttons -->
<div class="col-md-4">
<div class="social-buttons">
<ul>
<li class="gplus">
<a href="https://plus.google.com/+SttbandungAcIdBDG/about" rel="publisher" title="Google Plus">
<i class="fa fa-google-plus fa-fw"></i></a>
</a>
</li>
<li class="facebook">
<a href="https://id-id.facebook.com/pages/Sekolah-Tinggi-Teknologi-Bandung/230805698001" title="Facebook">
<i class="fa fa-facebook fa-fw"></i>
</a>
</li>
<li class="twitter">
<a href="https://twitter.com/STT_Bandung" title="Twitter">
<i class="fa fa-twitter fa-fw"></i>
</a>
</li>
<li class="flickr">
<a href="https://www.flickr.com/photos/123724897@N08/sets/72157644110311652/" title="Flickr Photostream">
<i class="fa fa-flickr fa-fw"></i>
</a>
</li>
<li class="rss">
<a href="http://sttbandung.ac.id/feed/" title="RSS">
<i class="fa fa-rss fa-fw"></i>
</a>
</li>
</ul>
</div>
</div>
<!-- //Social Buttons -->
</div>
<!-- Menu utama -->
<div class="container">
<div class="navbar navbar-default">
<div class="navbar-header">
<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
</div>
<div class="navbar-collapse collapse">
<?php
wp_nav_menu( array(
'menu'              => 'primary',
'theme_location'    => 'primary',
'depth'             => 2,
'container'         => 'div',
'container_class'   => 'collapse navbar-collapse',
'container_id'      => 'bs-example-navbar-collapse-1',
'menu_class'        => 'nav navbar-nav',
'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
'walker'            => new wp_bootstrap_navwalker())
);
?>
</div>
</div>
</div>
<!-- //Menu utama -->
<!-- Hero -->
<div class="container">
<div class="hero">
<h1>
<a href="<?php echo home_url(); ?>" title="<?php bloginfo('name'); ?> <?php bloginfo('description'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/STTB-L.png" alt="<?php bloginfo('name'); ?>"></a></h1>
<!-- searchform -->
<?php get_template_part('searchform'); ?>
<!-- /searchform -->
</div>
</div>
<!-- //Hero -->
<!-- Breadcrumb-->
<?php if(!is_home() && !is_page(21) && !is_page(80)):?>
<div class="breadcrumb">
<div class="container">
<?php if ( function_exists('yoast_breadcrumb')){yoast_breadcrumb('<p id="breadcrumbs">','</p>');}?>
</div>
</div>
<?php endif;?>
<!-- //breadcrumb -->
</header>