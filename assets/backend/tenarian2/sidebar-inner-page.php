<?php if ( is_active_sidebar( 'widget-page' ) ):?>
	<aside class="sidebar" role="complementary">	
		<div class="sidebar-widget">
			<?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('widget-page')) ?>
		</div>
	</aside>
<?php endif;?>