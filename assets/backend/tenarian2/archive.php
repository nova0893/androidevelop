<?php get_header();?>
	<div class="content-page search-result">
		<div class="container">
			<div class="col-md-12">
				<!-- section -->
				<section role="main">
					<header class="post-header">
						<h2><?php _e( 'Arsip', 'html5blank' ); ?></h2>
					</header>
			
					<?php get_template_part('loop'); ?>
			
					<?php get_template_part('pagination'); ?>
				</section>
				<!-- /section -->
			</div>
		</div>
	</div>
<?php get_footer();?>