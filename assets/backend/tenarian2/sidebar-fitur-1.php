<?php if ( is_active_sidebar( 'fitur-1' ) ) : ?>
	<!-- sidebar -->
	<aside class="sidebar" role="complementary">	
		<div class="sidebar-widget">
			<?php dynamic_sidebar( 'fitur-1' ); ?>
		</div>
		<hr>
	</aside>
	<!-- /sidebar -->
<?php endif;?>