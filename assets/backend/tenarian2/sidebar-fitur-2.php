<?php if ( is_active_sidebar( 'fitur-2' ) ):?>
	<hr>
	<aside class="sidebar" role="complementary">	
		<div class="sidebar-widget">
			<?php dynamic_sidebar( 'fitur-2' ); ?>
		</div>
	</aside>
<?php endif;?>