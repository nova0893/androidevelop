<?php if (have_posts()): while (have_posts()) : the_post();?>
	
	<!-- article -->	
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<?php if ( is_single() || is_page() ):?>
			<header class="entry-header">
				<!-- post title -->
				<h2>
					<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
				</h2>
			</header>
		<?php else: ?>
			<header class="entry-header entry-header-list">
				<h3>
					<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
				</h3>
			</header>
		<?php endif;?>
			<!-- /post title -->
		
			<!-- post details -->
			<span class="entry-author"><?php _e( 'Oleh ', 'html5blank' ); ?> <strong class="standout"><?php the_author(); ?></strong></span>
			<span class="entry-date"> | <?php the_time('l, d F Y'); ?> <?php the_time('g:i a'); ?></span>
			<!-- /post details -->
		</header>
		
		<div class="entry-content">
			<?php html5wp_excerpt('html5wp_index'); // Build your custom callback length in functions.php ?>
		</div>
	</article>
	<!-- /article -->
	
<?php endwhile; ?>

<?php else: ?>

	<!-- article -->
	<article>
		<div class="alert alert-warning">
			<h3><i class="fa fa-warning"></i> <?php _e( 'Maaf! Informasi yang Anda cari tidak dapat ditemukan!', 'html5blank' ); ?></h3>
		</div>
	</article>
	<!-- /article -->
<?php endif;?>