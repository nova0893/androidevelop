<div class="searchform pull-right">
	<form method="get" action="<?php echo home_url(); ?>" role="search">
		<button class="submit-search" type="submit"><i class="fa fa-search fa-fw grey"></i></button>
		<input class="input-search" placeholder="Cari..." type="text" value="" name="s" id="search">
	</form>
</div>