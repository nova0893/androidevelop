<?php get_header();?>
	<div class="page">
		<div class="container">
				<!-- section -->
				<section role="main">
					<div class="container">
						<h2 class="archive-title"><?php the_category(); ?></h2>
					</div>
					
					<div class="container">
						<?php get_template_part('loop'); ?>
					<?php get_template_part('pagination'); ?>
					</div>
				</section>
			<!-- /section -->
		</div>
	</div>
<?php get_footer();?>