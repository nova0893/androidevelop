<?php if ( is_active_sidebar( 'fitur-3' ) ):?>
	<aside class="sidebar" role="complementary">	
		<div class="sidebar-widget">
			<?php dynamic_sidebar( 'fitur-3' ); ?>
		</div>
	</aside>
<?php endif;?>