<!-- footer -->
	<div class="container">
		<footer class="footer" role="contentinfo">
			<div class="container">
				<?php
					wp_nav_menu( array(
						'menu'              => 'footer-menu',
						'theme_location'    => 'footer-menu',
						'depth'             => 2,
							'container'         => 'div',
							'container_class'   => 'footer-menu',
							'container_id'      => '',
							'menu_class'        => 'footer-nav',
							'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
							'walker'            => new wp_bootstrap_navwalker())
						);
					?>
					</div>
					
					<div class="container">
						<div class="col-md-6">
							<p>
								&copy; <?php echo date('Y')?> <span>sttbandung.ac.id</span>. Hak cipta dilindungi undang-undang.
							</p>
								
							<p>
								Jl. Soekarno Hatta No. 378 Bandung 40235 Jawa Barat
							</p>
						</div>
						
						<div class="col-md-6">
							<a href="<?php echo home_url(); ?>">
								<img src="<?php echo get_template_directory_uri(); ?>/img/logo-sttb-footer.svg" alt="<?php bloginfo('name'); ?>">
							</a>
						</div>
					</div>
				</footer>
			</div>
			<!-- //footer -->
		</div>
		
		<script>
			$(document).ready(function(){
				
				$('.carousel').carousel({
					interval: 4000
				});
				
				$("#menu-menu-atas").tinyNav();
				
				// Menambahkan ikon pada judul form komentar
				$('.comments h3#reply-title').prepend('<i class="fa fa-comments"></i> ');
			});
		</script>
		<?php wp_footer();?>	
	</body>
</html>