<?php get_header();?>
			<div class="container">
				<div class="col-md-8">
					
					<!-- Carousel -->
					<div class="container">	
						<div id="myCarousel" class="carousel slide">
							<!-- Indicators -->
							<ol class="carousel-indicators">
							  <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
							  <li data-target="#myCarousel" data-slide-to="1"></li>
							  <li data-target="#myCarousel" data-slide-to="2"></li>
							  <li data-target="#myCarousel" data-slide-to="3"></li>
							  <li data-target="#myCarousel" data-slide-to="4"></li>
							</ol>
						
							<div class="carousel-inner">
								<div class="item active">
									<img src="<?php echo get_template_directory_uri(); ?>/img/slide/slide-photo-2.jpg" alt="">
								</div>
								<div class="item">
									<img src="<?php echo get_template_directory_uri(); ?>/img/slide/slide-photo-1.jpg" alt="">
								</div>
								<div class="item">
									<img src="<?php echo get_template_directory_uri(); ?>/img/slide/slide-photo-3.jpg" alt="">
								</div>
								<div class="item">
									<img src="<?php echo get_template_directory_uri(); ?>/img/slide/slide-photo-4.jpg" alt="">
								</div>
								<div class="item">
									<img src="<?php echo get_template_directory_uri(); ?>/img/slide/slide-photo-5.jpg" alt="">
							  </div>
							</div>
						</div>
					</div>
					<!-- //Carousel -->
					
					<!-- Widget Fitur-1 -->
					<div class="container">
						<?php get_sidebar('fitur-1'); ?>	
					</div>
					<!-- //Widget Fitur-1 -->
					
					<!-- info sttb -->
					<div class="container">	
						<h3 class="block-title">Info STTB</h3>
					
						<?php $recent = new WP_Query("cat=3&showposts=5"); while($recent->have_posts()) : $recent->the_post();?>
								
							<header class="entry-header">
								<div class="entry-date">
									<?php the_time('l, d F Y')?>
								</div>
													
								<div class="entry-title">
									<a href="<?php the_permalink() ?>" rel="bookmark">
										<?php the_title(); ?>
									</a>
								</div>
							</header>
												
							<div class="entry-content">
								<?php html5wp_excerpt('html5wp_index');?>
							</div>
						<?php endwhile; ?>	
					
						<p>
							<a class="btn btn-sm btn-primary btn-square" href="<?php echo home_url(); ?>/category/info-sttb/">
								<i class="fa fa-folder"></i> Lihat Semua
							</a>
						</p>
					</div><!-- .single -->
					<!-- //info sttb -->
				
					<!-- Berita Foto -->
					<div class="container">	
						<hr>
						<h3 class="block-title">Foto Terbaru</h3>
					
						<?php 
							$recent = new WP_Query("cat=2&showposts=4"); 
							$trcol = 1;
							while($recent->have_posts()) : $recent->the_post();?>
							
								<div class="triple" id="triple-<?php echo $trcol++;?>">
								
									<header class="entry-header">
										<?php if ( has_post_thumbnail() ) : the_post_thumbnail('thumb-info'); ?><?php endif; ?>
									
										<div class="entry-date">
											<?php the_time('l, d F Y')?>
										</div>
										
										<div class="entry-title">
											<a href="<?php the_permalink() ?>" rel="bookmark">
												<?php the_title(); ?>
											</a>
										</div>
									</header>
								
								</div>
						<?php endwhile; ?>							
							
						<p style="clear:both">
							<a class="btn btn-sm btn-primary btn-square" href="<?php echo home_url(); ?>/category/berita-foto/">
								<i class="fa fa-folder"></i> Lihat Semua
							</a>
						</p>
					</div>
					<!-- //Berita Foto -->
					
					<!-- Widget Fitur-2 -->
					<div class="container">
						<?php get_sidebar('fitur-2'); ?>	
					</div>
					<!-- //Widget Fitur-2 -->
					
					
					<!-- Artikel Terbaru -->
					<div class="container">	
						<hr>
						<h3 class="block-title">Artikel Terbaru</h3>
					
						<?php 
							$recent = new WP_Query("cat=1&showposts=4"); 
							$dbcol = 1;
							while($recent->have_posts()) : $recent->the_post();?>
							
								<div class="double" id="double-<?php echo $dbcol++?>">								
									<header class="entry-header">
										<div class="entry-date">
											<?php the_time('l, d F Y')?>
										</div>
										
										<div class="entry-title">
											<a href="<?php the_permalink() ?>" rel="bookmark">
												<?php the_title(); ?>
											</a>
										</div>
									</header>
									
									<div class="entry-content">
										<?php html5wp_excerpt('html5wp_custom_post'); ?>
									</div>
								</div>
						<?php if($dbcol > 2){ $dbcol=1; echo '<div style="clear:left;"></div>';} endwhile; ?>							
							
						<p>
							<a class="btn btn-sm btn-primary btn-square" href="<?php echo home_url(); ?>/category/artikel/">
								<i class="fa fa-folder"></i> Lihat Semua
							</a>
						</p>
					</div>
					<!-- //Artikel Terbaru -->
					
					<!-- Widget Fitur-3 -->
					<div class="container">
						<?php get_sidebar('fitur-3'); ?>	
					</div>
					<!-- //Widget Fitur-3 -->
					
				</div><!-- .col-md-8 -->

				<!-- Sidebar Kanan -->
				<div class="col-md-4">
					<!-- Testimoni -->
					<div class="testimony">
					<h3 class="block-title">Testimoni</h3>
					
					<?php query_posts('category_name=testimoni&showposts=1&offset=0&random=true'); ?>
						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
							<header class="entry-header">
								<?php if ( has_post_thumbnail() ) : the_post_thumbnail('foto-alumni'); ?><?php endif; ?>
									<a class="entry-title" href="<?php the_permalink() ?>" rel="bookmark">
										<?php the_title(); ?>
									</a>
							</header>
							
							<div class="entry-content">
								<?php html5wp_excerpt('html5wp_index');?>
							</div>
						<?php endwhile; endif; ?>
						
<p><a class="btn btn-sm btn-primary btn-square" href="<?php echo home_url(); ?>/category/testimoni/">
		<i class="fa fa-folder"></i>  Lihat Semua </a>&nbsp;&nbsp;<a class="btn btn-sm btn-success btn-square" href="<?php echo home_url(); ?>/hubungi-kami/">
		<i class="fa fa-plus-circle"></i>  Kirim Testimoni </a></p>
					</div>
					<!-- //Testimoni -->
					
					<?php get_sidebar(); ?>			
				</div><!-- .col-md-4 -->
</div>
<?php get_footer();?>