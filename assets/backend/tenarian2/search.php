<?php get_header();?>
	<div class="content-page search-result">
		<div class="container">
			<div class="col-md-12">
				<!-- section -->
				<section role="main">
					<h2>
						<?php echo sprintf( __( 'Ditemukan %s hasil pencarian untuk kata kunci: ', 'html5blank' ), $wp_query->found_posts ); echo get_search_query(); ?>
					</h2>
					
					<?php get_template_part('loop'); ?>
				
					<?php get_template_part('pagination'); ?>
			
				</section>
				<!-- /section -->
			</div>
		</div>
	</div>
<?php get_footer();?>