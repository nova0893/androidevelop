<?php
/*Template Name: single_default*/
?>

<?php get_header(); ?>
	<div class="page">
		<div class="container">
			<div class="col-md-8">
				<!-- section -->

					<div class="container">
						<section role="main">
					<?php if (have_posts()): while (have_posts()) : the_post(); ?>
	
						<!-- article -->
						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<p class="catname"><?php _e( '', 'html5blank' ); the_category(', '); // Separated by commas ?></p>
			
							<header class="entry-header">
								<!-- post title -->
								<h2>
									<?php the_title(); ?>
								</h2>
								<!-- /post title -->
							
								<!-- post details -->
								<span class="entry-author"><?php _e( 'Oleh ', 'html5blank' ); ?> <strong class="standout"><?php the_author(); ?></strong></span>
								<span class="entry-date"> | <?php the_time('l, d F Y'); ?></span>
								<!-- /post details -->
							</header>
			
							<div class="entry-content entry-content-single">
								<?php the_content(); // Dynamic Content ?>
							</div>
			
							<footer class="entry-footer">
								<?php the_tags( __( 'Tags: ', 'html5blank' ), ', ', '<br>'); // Separated by commas with a line break at the end ?>			
								<?php edit_post_link(); // Always handy to have Edit Post Links available ?>			
							</footer>
							
							<?php comments_template( '', true ); ?>
						</article>
						<!-- /article -->
		
					<?php endwhile; ?>
	
					<?php else: ?>
	
						<!-- article -->
						<article>
							
							<h1><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h1>
							
						</article>
						<!-- /article -->
	
					<?php endif; ?>
	
				</section>
					</div>
		</div>
		
		<div class="col-md-4">
			<?php get_sidebar('inner'); ?>	
		</div>
	</div>
<?php get_footer();?>