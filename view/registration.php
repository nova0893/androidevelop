<?php
try{
  include '../config/koneksi.php';
	$query = "SELECT * FROM `ak_prodi`";
	$stmt = $pdo->prepare($query);
	$stmt->execute();
	$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);	
}catch(PDOException $ex) {
	echo "There are some problems in our database. Please try again later.";
	exit;
}
?>
    <?php include('header.php');?>

<style>.layout-top-nav .content-wrapper,.layout-top-nav .right-side,.layout-top-nav .main-footer{margin-left:0}</style>
 <section id="content" >
<body  cz-shortcut-listen="true">

 

    <!-- Full Width Column -->

      <div class="container">
        <!-- Content Header (Page header) -->
    
        
        <!-- Main content -->
        <section class="content">
          <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Pendaftaran Akun Baru</h3>
            </div><!-- /.box-header -->
            <!-- form start -->
            <form method="post" action="../controllers/user_registration.php">
                <div class="box-body">
                  <div class="form-group input-sm">
                      <label class="col-sm-2 col-xs-12 control-label">Program Studi</label>
                      <div class="col-sm-10 col-xs-12">
                          <select class="form-control input-sm" name="prodi">
                          <?php
						  	foreach($rows as $row) {
							  $alias = $row['str_alias'];
							  $name = $row['str_nm_prodi'];
							  echo '<option value="'.$alias.'">'.$name.'</option>';
							}
						  ?>
                          </select>
                      </div>
                  </div>
                  <div class="form-group input-sm">
                      <label class="col-sm-2 col-xs-12 control-label">NPM (Nomor Pokok)</label>
                      <div class="col-sm-10 col-xs-12">
                          <input type="text" class="form-control input-sm" name="npm" placeholder="NPM (Nomor Pokok)" required autocomplete="off">
                      </div>
                  </div>
                  <div class="form-group input-sm">
                      <label class="col-sm-2 col-xs-12 control-label">Nama</label>
                      <div class="col-sm-10 col-xs-12">
                          <input type="text" class="form-control input-sm" name="nama" placeholder="Nama Lengkap" required autocomplete="off">
                      </div>
                  </div>
                  <div class="form-group input-sm">
                      <label class="col-sm-2 col-xs-12 control-label">Email</label>
                      <div class="col-sm-10 col-xs-12">
                          <input type="text" class="form-control input-sm" name="email" placeholder="Email" required autocomplete="off">
                      </div>
                  </div>

                  <div class="form-group input-sm">
                      <label class="col-sm-2 col-xs-12 control-label">Tanggal Lahir</label>
                      <div class="col-sm-10 col-xs-12">
                          <input type="text" class="form-control input-sm dtepicker" name="tgllhr" placeholder="DD/MM/YYYY, cth. 30/01/1991" required autocomplete="off">
                      </div>
                  </div>
                </div>

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Daftar Akun</button>
                </div>
            </form>

          </div>
        </section>
        <!-- /.content -->
      </div>
      <!-- /.container -->
    </div>
    <!-- /.content-wrapper -->
    <?php include('footer.php');?>
  </div>
  <!-- ./wrapper -->
  

  <script type="text/javascript">
  	$(function(){
		$("#validateForm").submit(function(e) {
            e.preventDefault();
			
			var x = $('select[name="prodi"]').val();
			var y = $('input[name="npm"]').val();
			if(x == 'TI' && (y.substring(2,5) == "113" || y.substring(2,5) == "114")){
				$('#validateForm').unbind('submit');
				$('#validateForm').submit();
			}else if(x == 'TIF' && (y.substring(2,5) == "111" || y.substring(2,5) == "112")){
				$('#validateForm').unbind('submit');
				$('#validateForm').submit();
			}else if(x == 'DKV' && (y.substring(2,5) == "115" || y.substring(2,5) == "116")){
				$('#validateForm').unbind('submit');
				$('#validateForm').submit();
			}
			
			return false;
        });
	});
  </script>
  </section>
</body>
