<?php
ini_set('memory_limit', '-1');
?>

<?php
include '../../config/koneksi.php';
if (isset($_GET['id'])) {
    $query = $pdo->query("SELECT * FROM `ms_nilai` WHERE `id` = '$_GET[id]'");
    $data  = $query->fetch(PDO::FETCH_ASSOC);
} else {
    echo "NPM tidak tersedia!
<a href='index.php'>Kembali</a>";
    exit();
}
 
if ($data === false) {
    echo "Data tidak ditemukan!
<a href='index.php'>Kembali</a>";
    exit();
}
?>

<?php
include ("../../config/kon.php");
?>

<?php
$id = $data['id'];
$npm = $data['str_npm'];
$matkul = $data['nama_mkuliah'];
$nilai = $data['nilai'];
$keterangan = $data['keterangan'];

include '../../config/koneksi.php';

    $query = $pdo->query("SELECT * FROM `ms_mhs` WHERE `str_npm` = $npm");
    $data  = $query->fetch(PDO::FETCH_ASSOC);

?>

<style type="text/css">

	@font-face{font-family:"Esphimere";src:url("../../assets/font/esphimere/Esphimere.otf");}
	@font-face{font-family:"Esphimere";src:url("../../assets/font/esphimere/Esphimere Bold.otf");font-weight:bold;}
	@font-face{font-family:"Esphimere";src:url("../../assets/font/esphimere/Esphimere Italic.otf");font-style:italic;}
	*{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;}
	body{margin:0;padding:0;background-color: "ffffff"}
	body,input,select{font-family:"Esphimere";font-size:13px;}
	h1,h2,h3,h4,h5,p{margin:0;}
	.wrapper{width:1025px;margin:0 auto;text-align:center;}
	header{height:56px;}
	footer{height:100px;}
	#content{background-color:#fff;color:#636363;}
	header{background:url("../../assets/img/header-content.png") no-repeat left #82bf34;}
	footer{background:url("../../assets/img/header.png") no-repeat left #007ec8;}
	#sitetitle{color:#323094;font-size:21px;font-weight:400;padding:15px 0;}
	.bottom-sp{border-bottom:1px solid #e1e1e1;}
	.top-sp{border-top:1px solid #e1e1e1;}
	#content p, #footer p{padding:10px 0;line-height:16px;} 
	#content a{color:#018bd3;text-decoration:none;}
	footer .copyright{padding:30px 0;color:#e6ecef;}


	</style>
<html>
<head><title>OASIS ADMIN STT-BANDUNG</title>
<script src="//code.jquery.com/jquery.min.js"></script>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

	<link rel="stylesheet" href="../../assets/backend/dist/css/AdminLTE.min.css">
        <link rel="stylesheet" href="../../assets/backend/>dist/css/skins/_all-skins.min.css">
		<link rel="stylesheet" href="../../assets/backend/dist/css/skins/skin-purple.min.css">
        <script src="../../assets/backend/plugins/jQuery/jQuery-2.1.4.min.js"></script>

<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>


<style id="jsbin-css">
@media (min-width:768px) { 
  


  .nav-bg {
	    height: 0px;
	    width: 100%;
	    position: absolute;
	    top: 50px;
	    background: #e7e7e7;
	    transition: all .5s ease-in-out;
	}

	.menu-open .nav-bg { height: 50px } /* change to your height of the child menu */
  
	.menu-open #page {
        padding-top:60px;
	    transition: all .2s ease-in-out;
	}
  
  #page {      height:1000px;/* demo only */
}

	.navbar-nav.nav > li { position: static }

	.navbar-nav.nav .dropdown-menu {
	    left: 0 !important;
	    right: 0 !important;
	    box-shadow: none;
	    border: none;
	    margin: 0 auto;
	    max-width: 1170px;
	    background: transparent;
	    padding: 0;
	}

	.navbar-nav.nav .dropdown-menu > li { float: left }

	.navbar-nav.nav .dropdown-menu > li > a {
	    width: auto !important;
	    background: transparent;
	    line-height: 49px;
	    padding-top: 0;
	    padding-bottom: 0;
	    margin: 0;
	}
}

</style>
</head>
<body>
<header> 
            <div align="center">
            	</br>
	             <img src="../../assets/img/kecil.png"> 
    </div> 


    </header>	
     <div class="navbar navbar-default" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">OASIS-ADMIN</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="../home.php">Home</a></li>
            <li><a href="#pengumuuman">Update Pengumuman</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Input Nilai <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="inputnilaiti.php">Prodi. Teknik Industri</a></li>
                <li><a href="inputnilaitif.php">Prodi. Teknik Informatika</a></li>
                <li><a href="inputnilaidkv.php">Prodi. Desain Komunikasi Visual</a></li>
          </ul>
          </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Input Mata Kuliah<span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
          		<li><a href="../inputmkuliah/inputmkul_ti.php">Prodi. Teknik Industri</a></li>
                <li><a href="../inputmkuliah/inputmkul_tif.php">Prodi. Teknik Informatika</a></li>
                <li><a href="../inputmkuliah/inputmkul_dkv.php">Prodi. Desain Komunikasi Visual</a></li>
          </ul>

             <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Ubah Nilai <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                        <li><a href="../ubahnilaiti.php">Prodi. Teknik Industri</a></li>
                <li><a href="../ubahnilaitif.php">Prodi. Teknik Informatika</a></li>
                <li><a href="../ubahnilaidkv.php">Prodi. Desain Komunikasi Visual</a></li>
          </ul>
          </li>

          	<li><a href="../uploadinfo.php">Upload File</a></li>
            <li><a href="../about.php">About</a></li>
            <li><a href="../../controllers/logout.php">Logout</a></li>
         </ul>
        </div><!--/.nav-collapse -->
 </div>
 </div>
 <br>
	

 <section id="content" >
            <div class="container">
            <div align="center">
            <br>


  <form action="../../controllers/update_nilai.php?id=<?php echo $id?>" method="POST">
	<div class='row'>
		<div class="col-md">
			<div class="box box-info">
                <div class="box-header with-border">

					<h3 class="box-title"><i class='fa fa-users'></i>  Update Nilai <font color="#A52A2A"><b> <?php echo $data['str_nm_mhs']; ?> (</b></font> <font name="npm" color="#A52A2A" ><b><?php echo $data['str_npm']; ?></b></font> <font color="#A52A2A"><b> )</b></font>  <br><br> Prodi. Teknik Informatika</h3>
			
					</div>
					<div class="box-body">
					<br>
					<div class="form-horizontal">



									<div class="form-group">
										<label for="kategori" class="col-sm-4 control-label">Mata Kuliah</label>

										<div class="col-sm-5">
											 <label> <?php echo $matkul ?></label>
											
											
										</div>
									</div>
					</div>

										<?php if ($nilai === "A") : ?>
									
													<div class="form-horizontal">
									<div class="form-group">
										<label for="kategori" class="col-sm-4 control-label">Nilai</label>
										<div class="col-sm-5">
																<div class="btn-group" data-toggle="buttons">
											   <label class="btn btn-success">
											      <input type="radio" name="options" id="myoption1" value="A" checked>A
											   </label>
											   <label class="btn btn-success">
											         <input type="radio" name="options" id="myoption2" value="AB">AB
											   </label>
											   <label class="btn btn-success">
											         <input type="radio" name="options" id="myoption3" value="B">B
											   </label>
											   <label class="btn btn-success">
											         <input type="radio" name="options" id="myoption3" value="BC">BC
											   </label>
											   <label class="btn btn-success">
											         <input type="radio" name="options" id="myoption3" value="C">C
											   </label>
											   <label class="btn btn-success">
											         <input type="radio" name="options" id="myoption3" value="D">D
											   </label>
											   <label class="btn btn-danger">
											         <input type="radio" name="options" id="myoption3" value="E">E
											   </label>
											   <label class="btn btn-danger">
											         <input type="radio" name="options" id="myoption3" value="K">K
											   </label>
											   <label class="btn btn-warning">
											         <input type="radio" name="options" id="myoption3" value="T">T
											   </label>
											</div>
											</div>
											</div>


											<?php else :?>
           									 <?php if ($nilai === "AB") : ?>

           									 		<div class="form-horizontal">
									<div class="form-group">
										<label for="kategori" class="col-sm-4 control-label">Nilai</label>
										<div class="col-sm-5">
																<div class="btn-group" data-toggle="buttons">
											   <label class="btn btn-success">
											      <input type="radio" name="options" id="myoption1" value="A">A
											   </label>
											   <label class="btn btn-success">
											         <input type="radio" name="options" id="myoption2" value="AB" checked>AB
											   </label>
											   <label class="btn btn-success">
											         <input type="radio" name="options" id="myoption3" value="B">B
											   </label>
											   <label class="btn btn-success">
											         <input type="radio" name="options" id="myoption3" value="BC">BC
											   </label>
											   <label class="btn btn-success">
											         <input type="radio" name="options" id="myoption3" value="C">C
											   </label>
											   <label class="btn btn-success">
											         <input type="radio" name="options" id="myoption3" value="D">D
											   </label>
											   <label class="btn btn-danger">
											         <input type="radio" name="options" id="myoption3" value="E">E
											   </label>
											   <label class="btn btn-danger">
											         <input type="radio" name="options" id="myoption3" value="K">K
											   </label>
											   <label class="btn btn-warning">
											         <input type="radio" name="options" id="myoption3" value="T">T
											   </label>
											</div>
											</div>
											</div>

											<?php else :?>
           									 <?php if ($nilai === "B") : ?>

           									 		<div class="form-horizontal">
									<div class="form-group">
										<label for="kategori" class="col-sm-4 control-label">Nilai</label>
										<div class="col-sm-5">
																<div class="btn-group" data-toggle="buttons">
											   <label class="btn btn-success">
											      <input type="radio" name="options" id="myoption1" value="A">A
											   </label>
											   <label class="btn btn-success">
											         <input type="radio" name="options" id="myoption2" value="AB" >AB
											   </label>
											   <label class="btn btn-success">
											         <input type="radio" name="options" id="myoption3" value="B" checked>B
											   </label>
											   <label class="btn btn-success">
											         <input type="radio" name="options" id="myoption3" value="BC">BC
											   </label>
											   <label class="btn btn-success">
											         <input type="radio" name="options" id="myoption3" value="C">C
											   </label>
											   <label class="btn btn-success">
											         <input type="radio" name="options" id="myoption3" value="D">D
											   </label>
											   <label class="btn btn-danger">
											         <input type="radio" name="options" id="myoption3" value="E">E
											   </label>
											   <label class="btn btn-danger">
											         <input type="radio" name="options" id="myoption3" value="K">K
											   </label>
											   <label class="btn btn-warning">
											         <input type="radio" name="options" id="myoption3" value="T">T
											   </label>
											</div>
											</div>
											</div>

											<?php else :?>
           									 <?php if ($nilai === "BC") : ?>

           									 		<div class="form-horizontal">
									<div class="form-group">
										<label for="kategori" class="col-sm-4 control-label">Nilai</label>
										<div class="col-sm-5">
																<div class="btn-group" data-toggle="buttons">
											   <label class="btn btn-success">
											      <input type="radio" name="options" id="myoption1" value="A">A
											   </label>
											   <label class="btn btn-success">
											         <input type="radio" name="options" id="myoption2" value="AB" >AB
											   </label>
											   <label class="btn btn-success">
											         <input type="radio" name="options" id="myoption3" value="B">B
											   </label>
											   <label class="btn btn-success">
											         <input type="radio" name="options" id="myoption3" value="BC" checked>BC
											   </label>
											   <label class="btn btn-success">
											         <input type="radio" name="options" id="myoption3" value="C">C
											   </label>
											   <label class="btn btn-success">
											         <input type="radio" name="options" id="myoption3" value="D">D
											   </label>
											   <label class="btn btn-danger">
											         <input type="radio" name="options" id="myoption3" value="E">E
											   </label>
											   <label class="btn btn-danger">
											         <input type="radio" name="options" id="myoption3" value="K">K
											   </label>
											   <label class="btn btn-warning">
											         <input type="radio" name="options" id="myoption3" value="T">T
											   </label>
											</div>
											</div>
											</div>

											<?php else :?>
           									 <?php if ($nilai === "C") : ?>

           									 		<div class="form-horizontal">
									<div class="form-group">
										<label for="kategori" class="col-sm-4 control-label">Nilai</label>
										<div class="col-sm-5">
																<div class="btn-group" data-toggle="buttons">
											   <label class="btn btn-success">
											      <input type="radio" name="options" id="myoption1" value="A">A
											   </label>
											   <label class="btn btn-success">
											         <input type="radio" name="options" id="myoption2" value="AB" >AB
											   </label>
											   <label class="btn btn-success">
											         <input type="radio" name="options" id="myoption3" value="B">B
											   </label>
											   <label class="btn btn-success">
											         <input type="radio" name="options" id="myoption3" value="BC">BC
											   </label>
											   <label class="btn btn-success">
											         <input type="radio" name="options" id="myoption3" value="C" checked>C
											   </label>
											   <label class="btn btn-success">
											         <input type="radio" name="options" id="myoption3" value="D">D
											   </label>
											   <label class="btn btn-danger">
											         <input type="radio" name="options" id="myoption3" value="E">E
											   </label>
											   <label class="btn btn-danger">
											         <input type="radio" name="options" id="myoption3" value="K">K
											   </label>
											   <label class="btn btn-warning">
											         <input type="radio" name="options" id="myoption3" value="T">T
											   </label>
											</div>
											</div>
											</div>

											<?php else :?>
           									 <?php if ($nilai === "D") : ?>

           									 		<div class="form-horizontal">
									<div class="form-group">
										<label for="kategori" class="col-sm-4 control-label">Nilai</label>
										<div class="col-sm-5">
																<div class="btn-group" data-toggle="buttons">
											   <label class="btn btn-success">
											      <input type="radio" name="options" id="myoption1" value="A">A
											   </label>
											   <label class="btn btn-success">
											         <input type="radio" name="options" id="myoption2" value="AB" >AB
											   </label>
											   <label class="btn btn-success">
											         <input type="radio" name="options" id="myoption3" value="B">B
											   </label>
											   <label class="btn btn-success">
											         <input type="radio" name="options" id="myoption3" value="BC">BC
											   </label>
											   <label class="btn btn-success">
											         <input type="radio" name="options" id="myoption3" value="C">C
											   </label>
											   <label class="btn btn-success">
											         <input type="radio" name="options" id="myoption3" value="D" checked>D
											   </label>
											   <label class="btn btn-danger">
											         <input type="radio" name="options" id="myoption3" value="E">E
											   </label>
											   <label class="btn btn-danger">
											         <input type="radio" name="options" id="myoption3" value="K">K
											   </label>
											   <label class="btn btn-warning">
											         <input type="radio" name="options" id="myoption3" value="T">T
											   </label>
											</div>
											</div>
											</div>

											<?php else :?>
           									 <?php if ($nilai === "E") : ?>

           									 		<div class="form-horizontal">
									<div class="form-group">
										<label for="kategori" class="col-sm-4 control-label">Nilai</label>
										<div class="col-sm-5">
																<div class="btn-group" data-toggle="buttons">
											   <label class="btn btn-success">
											      <input type="radio" name="options" id="myoption1" value="A">A
											   </label>
											   <label class="btn btn-success">
											         <input type="radio" name="options" id="myoption2" value="AB" >AB
											   </label>
											   <label class="btn btn-success">
											         <input type="radio" name="options" id="myoption3" value="B">B
											   </label>
											   <label class="btn btn-success">
											         <input type="radio" name="options" id="myoption3" value="BC">BC
											   </label>
											   <label class="btn btn-success">
											         <input type="radio" name="options" id="myoption3" value="C">C
											   </label>
											   <label class="btn btn-success">
											         <input type="radio" name="options" id="myoption3" value="D" >D
											   </label>
											   <label class="btn btn-danger">
											         <input type="radio" name="options" id="myoption3" value="E" checked>E
											   </label>
											   <label class="btn btn-danger">
											         <input type="radio" name="options" id="myoption3" value="K">K
											   </label>
											   <label class="btn btn-warning">
											         <input type="radio" name="options" id="myoption3" value="T">T
											   </label>
											</div>
											</div>
											</div>

											<?php else :?>
           									 <?php if ($nilai === "K") : ?>

           									 		<div class="form-horizontal">
									<div class="form-group">
										<label for="kategori" class="col-sm-4 control-label">Nilai</label>
										<div class="col-sm-5">
																<div class="btn-group" data-toggle="buttons">
											   <label class="btn btn-success">
											      <input type="radio" name="options" id="myoption1" value="A">A
											   </label>
											   <label class="btn btn-success">
											         <input type="radio" name="options" id="myoption2" value="AB" >AB
											   </label>
											   <label class="btn btn-success">
											         <input type="radio" name="options" id="myoption3" value="B">B
											   </label>
											   <label class="btn btn-success">
											         <input type="radio" name="options" id="myoption3" value="BC">BC
											   </label>
											   <label class="btn btn-success">
											         <input type="radio" name="options" id="myoption3" value="C">C
											   </label>
											   <label class="btn btn-success">
											         <input type="radio" name="options" id="myoption3" value="D" >D
											   </label>
											   <label class="btn btn-danger">
											         <input type="radio" name="options" id="myoption3" value="E">E
											   </label>
											   <label class="btn btn-danger">
											         <input type="radio" name="options" id="myoption3" value="K" checked>K
											   </label>
											   <label class="btn btn-warning">
											         <input type="radio" name="options" id="myoption3" value="T">T
											   </label>
											</div>
											</div>
											</div>

											<?php else :?>
           									 <?php if ($nilai === "T") : ?>

           									 		<div class="form-horizontal">
									<div class="form-group">
										<label for="kategori" class="col-sm-4 control-label">Nilai</label>
										<div class="col-sm-5">
																<div class="btn-group" data-toggle="buttons">
											   <label class="btn btn-success">
											      <input type="radio" name="options" id="myoption1" value="A">A
											   </label>
											   <label class="btn btn-success">
											         <input type="radio" name="options" id="myoption2" value="AB" >AB
											   </label>
											   <label class="btn btn-success">
											         <input type="radio" name="options" id="myoption3" value="B">B
											   </label>
											   <label class="btn btn-success">
											         <input type="radio" name="options" id="myoption3" value="BC">BC
											   </label>
											   <label class="btn btn-success">
											         <input type="radio" name="options" id="myoption3" value="C">C
											   </label>
											   <label class="btn btn-success">
											         <input type="radio" name="options" id="myoption3" value="D" >D
											   </label>
											   <label class="btn btn-danger">
											         <input type="radio" name="options" id="myoption3" value="E">E
											   </label>
											   <label class="btn btn-danger">
											         <input type="radio" name="options" id="myoption3" value="K">K
											   </label>
											   <label class="btn btn-warning">
											         <input type="radio" name="options" id="myoption3" value="T" checked>T
											   </label>
											</div>
											</div>
											</div>

											 	<?php endif;  ?>
									            <?php endif;  ?>
									            <?php endif;  ?>
									            <?php endif;  ?>
									            <?php endif;  ?>
									            <?php endif;  ?>
									            <?php endif;  ?>
									            <?php endif;  ?>
									            <?php endif;  ?>

												<div class="form-horizontal">
									<div class="form-group">
										<label for="kategori" class="col-sm-4 control-label">Keterangan</label>
										<div class="col-sm-5">
											<input type="text" class="form-control input-sm" maxlength="100" name="keterangan" placeholder="Keterangan" required value="<?php echo $keterangan?>">

										</div>
									</div>
					</div>

					<div class="form-group">
					<div class='fa fa-search'>
								    <div class="col-sm-offset-3 col-sm-11">
								      	<button type="submit" class="btn btn-success"><i class="fa fa-search"></i> Simpan</button>
								    </div>
							  	</div>
							  	</div><br>




						</form>
						<br>
                  
              

						

					</div>
					</div>
					</div>
					</div>
				
					</section>

			
									

<br>
</br>
</div>
</div>
<?php
include("../back/footer.php");
?>



</section>

<script>
   
    $("#tahun").change(function(){
   
        // variabel dari nilai combo box provinsi
        var id_provinsi = $("#tahun").val();
        var awal = id_provinsi.slice(-2);
       
        // tampilkan image load
     
       
        // mengirim dan mengambil data
        $.ajax({
            type: "POST",
            dataType: "html",
            url: "search/cari_kelastif.php",
            data: "prov="+awal,
            success: function(msg){
               
                // jika tidak ada data
                if(msg == ''){
                    alert('Tidak ada data Kelas');
                }
               
                // jika dapat mengambil data,, tampilkan di combo box kota
                else{
                    $("#kelas").html(msg);                                                     
                }
               
                // hilangkan image load
               
            }
        });    
    });
</script>


</body>
</html>
