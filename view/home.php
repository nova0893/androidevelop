<?php
session_start();
if(!isset($_SESSION['name'])) {
   header('location:../index.php'); 
} else { 
   $name = $_SESSION['name']; 
}
?>

<?php
include ("header.php");
?>

<?php
include ("../config/koneksi.php");
?>

	

 <section id="content" >

            <div class="container">

            <div align="center">
    
<h1>Selamat Datang DI Halaman Admin <?php echo $name ?></h1>

<section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">

              <?php
                       $sql = "SELECT COUNT(oid) as qtymhs from `sttba789_siakad`.`ms_mhs` ";
                      $no  = 1;
                      foreach ($pdo->query($sql) as $data) :
                      	?>
              <h3><?php echo $data['qtymhs'] ?></h3>

              <p>Mahasiswa Melakukan Registrasi</p>
              <?php
              endforeach;
              ?>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="listmhs.php" class="small-box-footer"><font color="white">More info </font><i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
             <?php
                       $sql = "SELECT COUNT(id) as qtymkulti from `sttba789_siakad`.`ms_mkuliah` WHERE `str_nm_jur` = 'TI' ";
                      $no  = 1;
                      foreach ($pdo->query($sql) as $data) :
                      	?>
    
    
              <h3><?php echo $data['qtymkulti'] ?></h3>

              <p>Mata Kuliah Teknik Industri</p>

              <?php
              endforeach;
              ?>
            </div>
            <div class="icon">
              <i class="fa fa-tasks"></i>
            </div>
            <a href="listmti.php" class="small-box-footer"><font color="white">More info </font><i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">

              <?php
                       $sql = "SELECT COUNT(id) as qtymkultif from `sttba789_siakad`.`ms_mkuliah` WHERE `str_nm_jur` = 'TIF' ";
                      $no  = 1;
                      foreach ($pdo->query($sql) as $data) :
                      	?>
    
              <h3><?php echo $data['qtymkultif'] ?></h3>

              <p>Mata Kuliah Teknik Informatika</p>


                <?php
                    endforeach;
                    ?>  

            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="listmtif.php" class="small-box-footer"><font color="white">More info </font> <i class="fa fa-hand-o-down"></i></a>
          </div>
          

        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">

            <div class="inner">



                 <?php
                      $sql = "SELECT COUNT(id) as qtymkuldkv from `sttba789_siakad`.`ms_mkuliah` WHERE `str_nm_jur` = 'DKV' ";
                      $no  = 1;
                      foreach ($pdo->query($sql) as $data) :
                      	?>
              <h3><?php echo $data['qtymkuldkv'] ?></h3>

              <p>Mata Kuliah Desain Komunikasi Visual</p>

              <?php
              endforeach;
              ?>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="listmdkv.php" class="small-box-footer"><font color="white">More info </font> <i class="fa fa-arrow-circle-right"></i></a>
          </div>

        </div>



        <!-- ./col -->
      
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">

        <!-- Left col -->
   

        <!-- right col -->
      </div>
      <!-- /.row (main row) -->
 <div class="box-body table-responsive no-padding">



         
            </div>

             <?php
    if(isset($_GET['success']))
    {
        ?>
        <label>Mata kuliah berhasil diubah</label>
        <?php
    }
    else if(isset($_GET['fail']))
    {
        ?>
        <label>Terjadi masalah pada database. Cobalagi nanti</label>
        <?php
    }
    else
    {
        ?>
        <?php
    }
    ?>

    </section>

<br>
</br>
</div>
</div>
<?php
include("footer.php");
?>



</section>


</body>
</html>
