

<style type="text/css">

	@font-face{font-family:"Esphimere";src:url("../assets/font/esphimere/Esphimere.otf");}
	@font-face{font-family:"Esphimere";src:url("../assets/font/esphimere/Esphimere Bold.otf");font-weight:bold;}
	@font-face{font-family:"Esphimere";src:url("../assets/font/esphimere/Esphimere Italic.otf");font-style:italic;}
	*{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;}
	body{margin:0;padding:0;background-color: "ffffff"}
	body,input,select{font-family:"Esphimere";font-size:13px;}
	h1,h2,h3,h4,h5,p{margin:0;}
	.wrapper{width:1025px;margin:0 auto;text-align:center;}
	header{height:56px;}
	footer{height:100px;}
	#content{background-color:#fff;color:#636363;}
	header{background:url("../assets/img/header.png") no-repeat left #82bf34;}
	footer{background:url("../assets/img/header.png") no-repeat left #007ec8;}
	#sitetitle{color:#323094;font-size:21px;font-weight:400;padding:15px 0;}
	.bottom-sp{border-bottom:1px solid #e1e1e1;}
	.top-sp{border-top:1px solid #e1e1e1;}
	#content p, #footer p{padding:10px 0;line-height:16px;} 
	#content a{color:#018bd3;text-decoration:none;}
	footer .copyright{padding:30px 0;color:#e6ecef;}


	</style>
<html>
<head><title>OASIS ADMIN STT-BANDUNG</title>






<script src="//code.jquery.com/jquery.min.js"></script>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

	<link rel="stylesheet" href="../assets/backend/dist/css/AdminLTE.min.css">
        <link rel="stylesheet" href="../assets/backend/>dist/css/skins/_all-skins.min.css">
		<link rel="stylesheet" href="../assets/backend/dist/css/skins/skin-purple.min.css">
        <script src="../assets/backend/plugins/jQuery/jQuery-2.1.4.min.js"></script>

<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>



<style id="jsbin-css">
@media (min-width:768px) { 
  


  .nav-bg {
	    height: 0px;
	    width: 100%;
	    position: absolute;
	    top: 50px;
	    background: #e7e7e7;
	    transition: all .5s ease-in-out;
	}

	.menu-open .nav-bg { height: 50px } /* change to your height of the child menu */
  
	.menu-open #page {
        padding-top:60px;
	    transition: all .2s ease-in-out;
	}
  
  #page {      height:1000px;/* demo only */
}

	.navbar-nav.nav > li { position: static }

	.navbar-nav.nav .dropdown-menu {
	    left: 0 !important;
	    right: 0 !important;
	    box-shadow: none;
	    border: none;
	    margin: 0 auto;
	    max-width: 1170px;
	    background: transparent;
	    padding: 0;
	}

	.navbar-nav.nav .dropdown-menu > li { float: left }

	.navbar-nav.nav .dropdown-menu > li > a {
	    width: auto !important;
	    background: transparent;
	    line-height: 49px;
	    padding-top: 0;
	    padding-bottom: 0;
	    margin: 0;
	}
}

</style>
</head>
<body>
<header> 
        
         <div class="navbar-brand">
        <a href="home.php" >
        	<img  width="30px" height="30px" class="navbar-logo" src="../assets/img/logo-white.png"><font color="white" size="4"><b> OASIS ADMIN</b></font>
        </a>
        </div>
        <!--<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse"> <i class="fa fa-bars"></i> </button>-->
 


    </header>	
     <div class="navbar navbar-default" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>

          <font color="black">
          <a class="navbar-brand" href="home.php">BERANDA</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="registration.php">Daftar Mahasiswa</a></li>
            <li><a href="#pengumuuman">Update Pengumuman</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Input Nilai <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="inputnilaiti.php">Prodi. Teknik Industri</a></li>
                <li><a href="inputnilaitif.php">Prodi. Teknik Informatika</a></li>
                <li><a href="inputnilaidkv.php">Prodi. Desain Komunikasi Visual</a></li>
          </ul>
          </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Input Mata Kuliah<span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
          		<li><a href="inputmkul.php?jurusan=Teknik Industri">Prodi. Teknik Industri</a></li>
                <li><a href="inputmkul.php?jurusan=Teknik Informatika">Prodi. Teknik Informatika</a></li>
                <li><a href="inputmkul.php?jurusan=Desain Komunikasi Visual">Prodi. Desain Komunikasi Visual</a></li>
          </ul>

             <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Ubah Nilai <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="ubahnilaiti.php">Prodi. Teknik Industri</a></li>
                <li><a href="ubahnilaitif.php">Prodi. Teknik Informatika</a></li>
                <li><a href="ubahnilaidkv.php">Prodi. Desain Komunikasi Visual</a></li>
          </ul>
          </li>

          	<li><a href="uploadinfo.php">Upload File</a></li>
            <li><a href="about.php">About</a></li>
            <li><a href="../controllers/logout.php">Logout</a></li>
         </ul>
         </font>
        </div><!--/.nav-collapse -->
 </div>
 </div>
 <br>