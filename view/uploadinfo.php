

<?php
include ("header.php");
?>

<?php
include ("../config/dbconfig.php");
?>
	

 <section id="content" >
            <div class="container">
            <div align="center">
            <br>
<div class="box box-info">
                <div class="box-header with-border">

                    <h3 class="box-title"><i class='fa fa-users'></i> Upload File</h3>
                    </div>
                    <div class="box-body">
                    <br>
                    <div class="form-horizontal">


  <form action="../controllers/upload.php" method="post" enctype="multipart/form-data">
                   
                        <div class="form-group input-sm">
                      <label class="col-sm-2 control-label">Pengumuman</label>
                      <div class="col-sm-10 col-xs-12">
                          <input type="text" class="form-control input-sm" name="info" placeholder="Pengumuman" required autocomplete="off">
                      <br>


    <div class="form-group input-sm">
    <label class="col-sm-1">
    <input type="file" name="file" />
    </label>
    </div>

    <div class="form-group">
                    <div class='fa fa-search'>
                                    <div class="col-sm-offset-6 col-sm-10">
    <button type="submit" class="btn btn-success" name="btn-upload">Simpan</button>
    </div>
    </div>
    </div>
    </form>
    <br /><br />
    <?php
    if(isset($_GET['success']))
    {
        ?>
        <label>File berhasil disimpan</label>
        <?php
    }
    else if(isset($_GET['fail']))
    {
        ?>
        <label>Oops terjadi kesalahan!</label>
        <?php
    }
    else
    {
        ?>
        <?php
    }
    ?>

</div>

					</div>
					</div>
					</div>
				
					</section>
									

<br>
</br>
</div>
</div>
</div>
</div>
</div>
<?php
include("footer.php");
?>



</section>

<script>
   
    $("#tahun").change(function(){
   
        // variabel dari nilai combo box provinsi
        var id_provinsi = $("#tahun").val();
       
        // tampilkan image load
     
       
        // mengirim dan mengambil data
        $.ajax({
            type: "POST",
            dataType: "html",
            url: "search/cari_kelastif.php",
            data: "prov="+id_provinsi,
            success: function(msg){
               
                // jika tidak ada data
                if(msg == ''){
                    alert('Tidak ada data Kelas');
                }
               
                // jika dapat mengambil data,, tampilkan di combo box kota
                else{
                    $("#kelas").html(msg);                                                     
                }
               
                // hilangkan image load
               
            }
        });    
    });
</script>


</body>
</html>
