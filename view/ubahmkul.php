<?php
    $jurusan  = $_GET['jurusan'];
    $kode_mk  = $_GET['kodemk'];

?>

<?php


    if ($jurusan === "Teknik Industri"){
    	$jur = "TI";
    }
    else{
    	if ($jurusan === "Teknik Informatika"){
    		$jur = "TIF";
    	}else{
    		$jur = "DKV";
    	}
    }
?>


<?php
include '../config/koneksi.php';



if (isset($_GET['kodemk'])) {
    $query = $pdo->query("SELECT * FROM `ms_mkuliah` WHERE `str_kd_mk` = '$_GET[kodemk]'");
    $data  = $query->fetch(PDO::FETCH_ASSOC);
} else {
    echo "Kode Mata Kuliah tidak tersedia!";
    exit();
}
 
if ($data === false) {
    echo "Data tidak ditemukan!";
    exit();

}

		$id = $data['id'];
		$kd = $_GET['kodemk'];
		$jur = $data['str_nm_jur'];
		$nmk = $data['str_nm_mk'];
		$sks = $data['str_jml_sks'];

?>
<style type="text/css">

	@font-face{font-family:"Esphimere";src:url("../../assets/font/esphimere/Esphimere.otf");}
	@font-face{font-family:"Esphimere";src:url("../../assets/font/esphimere/Esphimere Bold.otf");font-weight:bold;}
	@font-face{font-family:"Esphimere";src:url("../../assets/font/esphimere/Esphimere Italic.otf");font-style:italic;}
	*{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;}
	body{margin:0;padding:0;background-color: "ffffff"}
	body,input,select{font-family:"Esphimere";font-size:13px;}
	h1,h2,h3,h4,h5,p{margin:0;}
	.wrapper{width:1025px;margin:0 auto;text-align:center;}
	header{height:56px;}
	footer{height:100px;}
	#content{background-color:#fff;color:#636363;}
	header{background:url("../../assets/img/header.png") no-repeat left #82bf34;}
	footer{background:url("../../assets/img/header.png") no-repeat left #007ec8;}
	#sitetitle{color:#323094;font-size:21px;font-weight:400;padding:15px 0;}
	.bottom-sp{border-bottom:1px solid #e1e1e1;}
	.top-sp{border-top:1px solid #e1e1e1;}
	#content p, #footer p{padding:10px 0;line-height:16px;} 
	#content a{color:#018bd3;text-decoration:none;}
	footer .copyright{padding:30px 0;color:#e6ecef;}


	</style>
<?php
include ("header.php");
?>
	

 <section id="content" >
            <div class="container">
            <div align="center">
            <br>



  <form action="../controllers/update_mkuliah.php" method="post">
	<div class='row'>
		<div class="col-md">
			<div class="box box-info">
                <div class="box-header with-border">

					<h3 class="box-title"><i class='fa fa-users'></i>  Ubah Mata Kuliah Prodi. <?php echo $jurusan?></h3>
			
					</div>
					<div class="box-body">
					<br>
					<div class="form-horizontal">

					<input type="hidden" name=jurusan value="<?php echo $jur?>"></input>
					<input type="hidden" name=id value="<?php echo $id?>"></input>


										<div class="form-horizontal">
									<div class="form-group">
										<label for="kategori" class="col-sm-4 control-label">Kode Mata Kuliah</label>

										<div class="col-sm-5">
											<input type="text" class="form-control input-sm" maxlength="100" name="kodematakuliah" required value="<?php echo $kd?>">

										</div>

										
									</div>
					</div>

					<div class="form-horizontal">
									<div class="form-group">
										<label for="kategori" class="col-sm-4 control-label">Nama Mata Kuliah</label>

										<div class="col-sm-5">
											<input type="text" class="form-control input-sm" maxlength="100" name="matakuliah" required value="<?php echo $nmk?>">

										</div>


										
									</div>
					</div>

					
											


												<div class="form-horizontal">
									<div class="form-group">
										<label for="kategori" class="col-sm-4 control-label">Jumlah SKS</label>

											<div class="col-sm-5">
											<input type="text" class="form-control input-sm" maxlength="100" name="sks" required value="<?php echo $sks?>">

										</div>
									</div>
					</div>

					<div class="form-group">
					<div class='fa fa-search'>
								    <div class="col-sm-offset-3 col-sm-11">
								      	<button type="submit" class="btn btn-success"><i class="fa fa-search"></i> Simpan</button>
								    </div>
							  	</div>
							  	</div><br>

						</form>
						<br>
                   
           

						

					</div>
					</div>
					</div>
					</div>
				
					</section>
									

<br>
</br>
</div>
</div>
<?php
include ("footer.php");
?>



</section>




</body>
</html>
