<?php

include('../config/koneksi.php');
include('header.php');

//for total count data
$query = $pdo->prepare( "SELECT COUNT(str_nm_jur) FROM ms_mkuliah WHERE str_nm_jur = 'TIF'");  
$query->execute(); 

while($r = $query->fetch()) {
   $total_records = $r[0];
 } 
  
$total_pages = ceil($total_records / $limit);

//for first time load data
if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };  
$start_from = ($page-1) * $limit; 


$query = $pdo->prepare ("SELECT * FROM ms_mkuliah WHERE str_nm_jur = 'TIF' ORDER BY `str_nm_mk` ASC LIMIT $start_from, $limit");  
$query->execute(); ?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css">
<script type="text/javascript" charset="utf8" src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-2.0.3.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="dist/simplePagination.css" />
<script src="dist/jquery.simplePagination.js"></script>


<title></title>
<script>
</script>
</head>
<body>


<div class="container" >

<h1>Daftar Mahasiswa</h1>

  <form action="" method="get">
            <input type="text" class="form-control input-sm" maxlength="8" name="search" required autocomplete="off" placeholder="Cari Berdasarkan Nama Mata Kuliah">
</br>                   


          <div class='fa fa-search'>
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-success"><i class="fa fa-search"></i> Cari Data</button>
                    </div>
                  </div>
                  </br></br>
          </form>


<table class="table table-bordered table-striped">  
<thead>  
<tr>  
<th>No</th>  
<th>Kode Mata Kuliah</th>
<th>Mata Kuliah</th>
<th>Jumlah SKS</th>
</tr>  
</thead>  
<tbody id="target-content">


 <?php 
 $no = 1;

       
      if(isset($_GET['search'])){
                        
                           $param = $_GET['search'];
                           $query = $pdo->prepare("SELECT * FROM ms_mkuliah  WHERE WHERE str_nm_jur = 'TIF' AND  `str_kd_mk` LIKE :param  OR `str_nm_mk` LIKE :param ORDER BY `str_nm_mk` ASC LIMIT $start_from, $limit" );
                           $query->bindValue(':param', '%'.$param.'%', PDO::PARAM_STR);
                           $query->execute();
                           if($query->rowCount() > 0 ){
                                
                               $no=1;
                               while ($r = $query->fetch()) {
                                    
                                   echo '<tr>
                                            
                                            <td>'.$no.'</td>
                                            <td>'.$r['str_kd_mk'].'</td>
                                            <td>'.$r['str_nm_mk'].'</td>
                                            <td>'.$r['str_jml_sks'].'</td>
                                            <td align="center"> <a href="ubahmkul.php?jurusan=Teknik Informatika & kodemk='.$r['str_kd_mk'].'">'.'Ubah </a>'.'</td>   
                      
                                         </tr>';
                                    
                                   ++$no;
                    
                                }//end while
                                
                            }else{
                                
                                echo "<tr><td colspan=\"4\">Not Found</td></tr>";
                            }
                            
                        }//end if

                               while ($r = $query->fetch()) {
                                    
                                   echo '<tr>
                                            
                                            <td>'.$no.'</td>
                                            <td>'.$r['str_kd_mk'].'</td>
                                            <td>'.$r['str_nm_mk'].'</td>
                                            <td>'.$r['str_jml_sks'].'</td>  
                                            <td align="center"> <a href="ubahmkul.php?jurusan=Teknik Informatika & kodemk='.$r['str_kd_mk'].'">'.'Ubah </a>'.'</td> 
                      
                      
                                         </tr>';
                                   ++$no;
                    
                                }//end while
                                
                    
                    
                    ?>


</tbody> 
</table>
<nav><ul class="pagination">
<?php if(!empty($total_pages)):for($i=1; $i<=$total_pages; $i++):  
            if($i == 1):?>
            <li class='active'  id="<?php echo $i;?>"><a href='vpagination/paginationtif.php?page=<?php echo $i;?>'><?php echo $i;?></a></li> 
            <?php else:?>
            <li id="<?php echo $i;?>"><a href='vpagination/paginationtif.php?page=<?php echo $i;?>'><?php echo $i;?></a></li>
        <?php endif;?>          
<?php endfor;endif;?>
</ul></nav>
</div>
</body>
<script type="text/javascript">
$(document).ready(function(){
$('.pagination').pagination({
        items: <?php echo $total_records;?>,
        itemsOnPage: <?php echo $limit;?>,
        cssStyle: 'light-theme',
    currentPage : 1,
    onPageClick : function(pageNumber) {
      jQuery("#target-content").html('loading...');
      jQuery("#target-content").load("vpagination/paginationtif.php?page=" + pageNumber);
    }
    });
});
</script>

<?php
include('footer.php');
?>
?<!--  -->>