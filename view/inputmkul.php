<?php
    $jurusan  = $_GET['jurusan'];

?>

<?php


    if ($jurusan === "Teknik Industri"){
    	$jur = "TI";
    }
    else{
    	if ($jurusan === "Teknik Informatika"){
    		$jur = "TIF";
    	}else{
    		$jur = "DKV";
    	}
    }
?>
<style type="text/css">

	@font-face{font-family:"Esphimere";src:url("../../assets/font/esphimere/Esphimere.otf");}
	@font-face{font-family:"Esphimere";src:url("../../assets/font/esphimere/Esphimere Bold.otf");font-weight:bold;}
	@font-face{font-family:"Esphimere";src:url("../../assets/font/esphimere/Esphimere Italic.otf");font-style:italic;}
	*{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;}
	body{margin:0;padding:0;background-color: "ffffff"}
	body,input,select{font-family:"Esphimere";font-size:13px;}
	h1,h2,h3,h4,h5,p{margin:0;}
	.wrapper{width:1025px;margin:0 auto;text-align:center;}
	header{height:56px;}
	footer{height:100px;}
	#content{background-color:#fff;color:#636363;}
	header{background:url("../../assets/img/header.png") no-repeat left #82bf34;}
	footer{background:url("../../assets/img/header.png") no-repeat left #007ec8;}
	#sitetitle{color:#323094;font-size:21px;font-weight:400;padding:15px 0;}
	.bottom-sp{border-bottom:1px solid #e1e1e1;}
	.top-sp{border-top:1px solid #e1e1e1;}
	#content p, #footer p{padding:10px 0;line-height:16px;} 
	#content a{color:#018bd3;text-decoration:none;}
	footer .copyright{padding:30px 0;color:#e6ecef;}


	</style>
<?php
include ("header.php");
?>
	

 <section id="content" >
            <div class="container">
            <div align="center">
            <br>



  <form action="../controllers/insert_mkuliah.php" method="post">
	<div class='row'>
		<div class="col-md">
			<div class="box box-info">
                <div class="box-header with-border">

					<h3 class="box-title"><i class='fa fa-users'></i>  Input Mata Kuliah Prodi. <?php echo $jurusan?></h3>
			
					</div>
					<div class="box-body">
					<br>
					<div class="form-horizontal">

					<input type="hidden" name=jurusan value="<?php echo $jur?>"></input>


										<div class="form-horizontal">
									<div class="form-group">
										<label for="kategori" class="col-sm-4 control-label">Kode Mata Kuliah</label>
										<div class="col-sm-5">
											<input type="text" class="form-control input-sm" maxlength="9" name="kodematakuliah" placeholder="Kode Mata Kuliah" required autocomplete="off">
										</div>
									</div>
					</div>

					<div class="form-horizontal">
									<div class="form-group">
										<label for="kategori" class="col-sm-4 control-label">Nama Mata Kuliah</label>
										<div class="col-sm-5">
											<input type="text" class="form-control input-sm" maxlength="100" name="matakuliah" placeholder="Nama Mata Kuliah" required autocomplete="off">
										</div>
									</div>
					</div>

					
											


												<div class="form-horizontal">
									<div class="form-group">
										<label for="kategori" class="col-sm-4 control-label">Jumlah SKS</label>
										<div class="col-sm-5">
											<input type="text" class="form-control input-sm" name="sks" maxlength="1" placeholder="Jumlah SKS" required autocomplete="off">
										</div>
									</div>
					</div>

					<div class="form-group">
					<div class='fa fa-search'>
								    <div class="col-sm-offset-3 col-sm-11">
								      	<button type="submit" class="btn btn-success"><i class="fa fa-search"></i> Simpan</button>
								    </div>
							  	</div>
							  	</div><br>

						</form>
						<br>
                   
            <?php
    if(isset($_GET['data']))
    {
        ?>
        <label>Mata Kuliah Berhasil Ditambahkan</label>
        <?php
    }
    else if(isset($_GET['fail']))
    {
        ?>
        <label>Oops terjadi kesalahan!</label>
        <?php
    }
    else
    {
        ?>
        <?php
    }
    ?>

						

					</div>
					</div>
					</div>
					</div>
				
					</section>
									

<br>
</br>
</div>
</div>
<?php
include ("footer.php");
?>



</section>




</body>
</html>
